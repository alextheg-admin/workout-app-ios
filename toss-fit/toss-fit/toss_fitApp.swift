//
//  toss_fitApp.swift
//  toss-fit
//
//  Created by Alex Theg on 8/27/21.
//

import SwiftUI

@main
struct toss_fitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
